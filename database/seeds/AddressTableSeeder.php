<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Address;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0; $i<100; $i++){
        	$user_id = User::all()->shuffle()->first()->id;
        	Address::create([
        		'user_id' => $user_id,
        		'address1' => $faker->streetAddress(),
        		'city' => $faker->city(),
        		'state' => $faker->state(),
        		'zip' => $faker->postcode(),
        		'photo_url' => $faker->imageUrl($width = 640, $height = 480)
        	]);
        }
    }
}
