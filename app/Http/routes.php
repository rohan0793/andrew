<?php

use Illuminate\Http\Request;

Route::get('test', function(){
	return view('test');
});

Route::get('test2', function(){
	$users = App\User::all();
	$users->load('addresses');

	return $users;
});

Route::get('auth/login', function(){
	if(Auth::user()->id){
		return redirect()->intended('/addresses');
	}

	return view('login');
});

Route::post('auth/login', function(Request $request){
	$data = $request->all();

	if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
        // Authentication passed...
        return redirect()->intended('/addresses');
    }
});

Route::group(['prefix' => 'api'], function(){
	Route::resource('addresses', 'AddressController');
});

Route::get('addresses', function(){
	return view('address.index');
});