<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" href="/assets/bower/bootstrap/dist/css/bootstrap.min.css" />

	<link rel="stylesheet" href="/assets/bower/bootstrap/dist/js/bootstrap.min.js" />
</head>
<body>

	<div style="height:100px"></div>

	<div class="container">
		<div class="col-xs-12 col-md-6 col-md-offset-3">
			<form method="post" action="/auth/login">
				<div class="form-group">
					<label for="exampleInputEmail1">Email address</label>
					<input name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Password</label>
					<input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button type="submit" class="btn btn-default">Login</button>
			</form>
		</div>
	</div>

</body>
</html>