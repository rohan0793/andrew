<html>
<head>
	<title>Addresses</title>

	<link rel="stylesheet" href="/assets/bower/kendo-ui/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="/assets/bower/kendo-ui/styles/kendo.default.min.css" />
    <link rel="stylesheet" href="/assets/bower/bootstrap/dist/css/bootstrap.min.css" />

    <script src="/assets/bower/kendo-ui/js/jquery.min.js"></script>
    <script src="/assets/bower/kendo-ui/js/kendo.ui.core.min.js"></script>
    <link rel="stylesheet" href="/assets/bower/bootstrap/dist/js/bootstrap.min.js" />
</head>
<body>

	<div class="container">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul id="listView"></ul>
				</div>
			</div>
		</div>
	</div>

	<script type="text/x-kendo-template" id="addressTemplate">
		#= address1 # <br />
		#= city # <br />
		#= state # <br />
		#= zip # <br />
		<img src="#= photo_url #"> <hr />
	</script>

	<script type="text/javascript">

		var template = new kendo.template($('#addressTemplate').html());

		var dataSource = new kendo.data.DataSource({
			transport: {
				read: '/api/addresses',
			},
			change: function(){
				$('#listView').html(kendo.render(template, this.view()));
			}
		});

		$("#listView").kendoListView({
			template: template,
			dataSource: dataSource
		});
	</script>

</body>
</html>