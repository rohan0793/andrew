<html>
<head>
	<title>Testing</title>

	<link rel="stylesheet" href="assets/bower/kendo-ui/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="assets/bower/kendo-ui/styles/kendo.default.min.css" />

    <script src="assets/bower/kendo-ui/js/jquery.min.js"></script>
    <script src="assets/bower/kendo-ui/js/kendo.ui.core.min.js"></script>
</head>
<body>
	<div class="table-responsive">
        <table class="table">
            <thead>
                <th>UserId</th>
                <th>Id</th>
                <th>Title</th>
                <th>Body</th>
            </thead>
            <tbody id="posts">

            </tbody>
        </table>
    </div>

    <script type="text/x-kendo-templete" id="myTemplate">
            <tr>
                <td>#= userId #</td>
                <td>#= id #</td>
                <td>#= title #</td>
                <td>#= body #</td>
            </tr>
    </script>

    <script type="text/javascript">
        var myTemplate = kendo.template($('#myTemplate').html());

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: 'http://jsonplaceholder.typicode.com/posts'
            },
            change: function(){
                $('#posts').html(kendo.render(myTemplate, this.view()));
            }
        });

        dataSource.read();
    </script>
</body>
</html>